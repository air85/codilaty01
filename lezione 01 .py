### 1.1. For loops


for i in range(0, 100):
 print (i)
 
#### Esempio 1.1.1
n=5
factorial = 1
for i in range (1, n + 1):
  factorial *= i

print (factorial) 


#### Esempio 1.1.2
### 1.2.A

n=3

for i in range(1, n + 1):
  for j in range(i):
    print  (i, end="")
  print  (j)
  
### 1.1.2.B

n=5

for i in range(1, n + 1):
  for j in range(i):
    print  ("*", end="")
  print  ("")

### 1.1.2.C
n=5
for i in range(n, 0, -1):
    for j in range(n - i):
      	print (" ", end = "")
    for j in range(2 * i - 1):
    	print ("*", end = "")
    print ()
	
### 1.1.2.C  DIAMANTE 
n=5

for i in range(1, n):
    for j in range(n - i):
      	print (" ", end = "")
    for j in range(2 * i - 1):
    	print ("*", end = "")
    print ()

#print  ("")
for i in range(n, 0, -1):
    for j in range(n - i):
      	print (" ", end = "")
    for j in range(2 * i - 1):
    	print ("*", end = "")
    print ()
	
### 1.1.2.C  LANCIA/FRECCIA

n=5
### linea dritta centale N x 3
for i in range(0, n*3):
	for i in range(1, n):
		print (" ", end = "")
	print ("*", end = "")
	print  ("")

for i in range(1, n):
    for j in range(n - i):
      	print (" ", end = "")
    for j in range(2 * i - 1):
    	print ("*", end = "")
    print ()

#print  ("")
for i in range(n, 0, -1):
    for j in range(n - i):
      	print (" ", end = "")
    for j in range(2 * i - 1):
    	print ("*", end = "")
    print ()

### 1.2.1 
### Fibonacci number
n=100

a = 0
b = 1
while a <= n:
	print (a)
	c = a + b
	a = b
	b = c

### 1.3.1 

days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday','Friday', 'Saturday', 'Sunday']

for day in days:
	print (day)
	
### 1.3.2 SET
	
days = set(['Monday', 'Tuesday', 'Wednesday', 'Thursday','Friday', 'Saturday', 'Sunday'])

for day in days:
	print (day)
	
### 1.3.3
days = {'mon':'Monday','tue':'Tuesday', 'wed':'Wednesday', 'thu':'Thursday','fri':'Friday', 'sat':'Saturday', 'sun':'Sunday'}

for day in days:
	print (day, "stands for ", days[day])	

